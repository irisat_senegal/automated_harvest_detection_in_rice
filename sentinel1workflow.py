# -*- coding: utf-8 -*-
"""
Created on Fri Jul 15 11:31:56 2022

@author: Glorie M. WOWO

@Description : 

"""
#========================================================================
#-----------------------  IMPORT PACKAGES
#========================================================================

import os
from glob import glob
import matplotlib.pyplot as plt
import rasterio as rio
from rasterio.plot import show_hist
from rasterio.plot import plotting_extent
import geopandas as gpd
import earthpy as et
import earthpy.spatial as es
import earthpy.plot as ep
from rasterio.plot import show
import pandas as pd
import numpy.ma as ma
import numpy as np
from rasterstats import zonal_stats
from rasterio.mask import mask
import seaborn as sns
from rasterio.features import dataset_features

from rasterio.merge import merge
from datetime import datetime, date, timedelta



#========================================================================
#-----------------------  USEFUL  FUNCTION
#========================================================================

#------------------------------------------------------------------

def from_doy_to_date(doy, year):
    """
        Description:
          Convert the day of year into dates
        Args:
          doy(int): the day of year
          year(int): The current
        Returns:
          Returns date in the following format : %Y-%m-%d
    """
    strt_date = date(int(year), 1, 1)
    res_date = strt_date + timedelta(days=int(doy) - 1)
    res = res_date.strftime("%Y-%m-%d")
    return res
    
#------------------------------------------------------------------
   
def from_date_to_doy(x):
  return datetime.strptime(str(x) ,"%Y-%m-%d").timetuple().tm_yday

#------------------------------------------------------------------

def create_folder(path):
  """
    Description:
      Create a folder in the given path
    Args:
      path(string): path
    Returns:
  """
  # Check whether the specified path exists or not
  isExist = os.path.exists(path)
  if not isExist:
  # Create a new directory because it does not exist 
    os.makedirs(path)
  else:
    pass

#------------------------------------------------------------------

def getFeatures(gdf):
    """
    Function to parse features from GeoDataFrame in such a manner that rasterio wants them
    """
    import json
    return [json.loads(gdf.to_json())['features'][0]['geometry']]

#------------------------------------------------------------------


def crop_img( img_path, gdf ):
    """
    """
  # gdf = gpd.read_file( contour_path )
    coords = getFeatures( gdf )
    f = rio.open(img_path , 'r')
    out_img, out_transform = mask(f, shapes=coords, crop= True)
    out_img = out_img.reshape( out_img.shape[1], out_img.shape[2] )
  # out_img [out_img == 0] = np.nan
    return [out_img, out_transform]
    
#------------------------------------------------------------------
def get_images_path(path):
    
    path_ = glob(
      os.path.join(
          "data",
          "vignette-landsat",
          path,
      )
    )
    path_.sort()
    return path_

#------------------------------------------------------------------



def generate_map(data_path, gdfi, operation):
  start_date = [ path.split('/')[-1].split('.')[0].split('_')[1] for path in data_path ]
  start_date_doy = [ from_date_to_doy(x) for x in start_date ]
  end_date = [ path.split('/')[-1].split('.')[0].split('_')[2] for path in data_path ]
  name =  [ path.split('/')[-1].split('.')[0]  for path in data_path ]

  #=======================================================
  start_date = [ from_date_to_doy(x) for x in  start_date ]
  
  #=======================================================
  df = pd.DataFrame()
  i =0
  for pat in data_path:

    crop_img_output = crop_img (pat, gdfi )
    
    df[start_date[ i ]] = crop_img_output[0].ravel() # rio.open(pat).read().ravel()
    i =i+1


  tmp = rio.open(data_path[0])  #crop_img( data_path[0], gdfi )  # 
  array = tmp.read()
  # print(array.shape, df.shape)
  tmp= crop_img( data_path[0], gdfi )
  #===================================================
  #=================== FLOODING=======================
  if operation == 'flooding':
    minValueIndex1 = df [ start_date ] .idxmin(axis=1)
    minValues = df[ start_date ].min(axis = 1)

    #===================================================
    df['start_season'] = minValueIndex1 
    # df['start_season_val'] = minValues

    arra = df[['start_season']].to_numpy()
  elif operation  == 'harvest':
    #===================HARVEST ==========================
    maxValueIndex1 = df [ start_date ].idxmax(axis=1)
    maxValues = df[ start_date ].max(axis = 1)
    df['end_season'] = maxValueIndex1 
    # df['end_season_val'] = maxValues 
    arra = df[['end_season']].to_numpy()
    # df['difference_val']  = maxValues / minValues 
    # df['difference_val']  = maxValues - minValues 
    #==============================================

  newarr2 = arra.reshape( tmp[0].shape[0], tmp[0].shape[1] )  #reshape(arra.shape[0], arra.shape[1])
  ar2 = ma.asarray(newarr2)
 

  return [ar2, tmp[1]]


#========================================================#


def stats_(df1, array, affine ):
  kallio = df1.copy()

  zs_kallio = zonal_stats( kallio, array, affine=affine, categorical=True )  #['min', 'max', 'mean', 'median', 'majority'])

  return zs_kallio
#------------------------------------------------------------------



# FIll DF inline 
def processing (grid_path, images_path,save_maps_path, operation='harvest' ):

  # data_path = get_images_path(path)
  data_path = images_path
  df = gpd.read_file(grid_path)


  df_output =  pd.DataFrame( ) #columns= columns_)
  list_img = []
  
  for j in range(df.shape[0]):  #range (5):  #
    id_ = df.iloc[j].name
    dftmp = df[j:j+1]

    maps_output = generate_map(data_path, dftmp, operation) 

    array =  maps_output[0]
    list_img.append(array)
    affine =  maps_output[1]  #rt[1]   

    df_inter =  stats_(dftmp, array, affine)
    df_inter=pd.DataFrame(df_inter)
    #pd.DataFrame( [ val ] )

    df_inter = df_inter.rename( index={0: id_} )

    df_output = pd.concat( [ df_output, df_inter ] )

      #------------------SAVE MAPS -----------------
    # if save_maps_path!=False:
    profile= rio.open(data_path[0]).profile
    profile.update(
            {"driver": "GTiff",
                "height": array .shape[0],
                "width": array .shape[1],
                "transform":affine,
            }
        )
        
    img_name=   save_maps_path+'/grid_'+str(id_) +'.tif'

    with rio.open(img_name, 'w', **profile) as dst:
                dst.write(array.astype(rio.uint16), 1) 

  return ([ df_output , list_img])
  
  
  
  #========================================================================
  #=======================================================================
class MainS1(object):
    
  def __init__(self,grid_path, images_path,output_path, year, operation='flooding'):
      
    self.grid_path = grid_path
    self.images_path = images_path
    self.output_path = output_path
    self.operation = operation
    self.year = year

  def result(self):
    #   output_path = self.output_path
    # For Grid
      create_folder(self.output_path+'/rasters')
      create_folder(self.output_path+'/rasters/grids')
      save_maps_path = self.output_path+'/rasters/grids'
    # For DF
      create_folder(self.output_path+'/dataframe')
      
      res = processing(self.grid_path, self.images_path,save_maps_path, operation = self.operation)

      gdf_path = self.output_path+'/dataframe/'+self.operation+'.geojson'
      df_path = self.output_path+'/dataframe/'+self.operation+'.csv'

      gdf  = gpd.read_file(self.grid_path)
      df_pts = res[0]
      
      df_pts = df_pts*100*0.0001
      cols_ = [from_doy_to_date(int(x) , self.year) for x in df_pts.columns.to_list()]

      df_pts = df_pts.rename(columns = dict(zip( df_pts.columns.to_list() , cols_)))
      #-==========================================
      # For now. we could take the range between the 2nd and the 4th quartile ? 
    #   maxValueIndex1 = df_pts[ cols_[1:] ].idxmax(axis=1)  # This was for MNDWI
      maxValueIndex1 = df_pts[cols_ ].idxmax(axis=1)

      df_pts [self.operation+'_date'] = maxValueIndex1 

      #-========================================
      gdf[df_pts.columns.to_list()] = df_pts
      

      gdf.to_file(gdf_path, driver = 'GeoJSON')
      gdf.to_csv(df_path)

      #--------------------------MERGE OUTPUT----------------------------
      #==================================================================
      path_ = save_maps_path +"/grid_*.tif"
      output_path_map=  self.output_path+'/rasters/'+self.operation+'map.tif'

      raster_files = get_images_path(path_ )
      # Check if there more than a file in the grid folder
      merge_grid = len(raster_files)
      
      if merge_grid >= 1:  # We can merge the grid
          raster_to_mosiac = []
          for files in raster_files:
            raster = rio.open(files)
            raster_to_mosiac.append(raster)
          # raster_to_mosiac = res[2].copy()
          mosaic, output = merge(raster_to_mosiac)

          # SAVE 

          output_meta = raster.meta.copy()
          output_meta.update(
              {"driver": "GTiff",
                  "height": mosaic.shape[1],
                  "width": mosaic.shape[2],
                  "transform": output,
              }
          )
          with rio.open(output_path_map, 'w', **output_meta) as m:
            m.write(mosaic.astype(rio.uint16))
      return gdf
      
      
#================================================================
#----------------- POST PROCESSING FUNCTION --------------------
#===============================================================

#---------------------------------------------------------------
def create_proportion( rgb_img, data_path,proportion_path, year):
  '''
  '''
  count_ref_img1 = zonal_stats(data_path,rgb_img,categorical=True)
  stat_df = pd.DataFrame(count_ref_img1)
  geogrid = gpd.read_file (data_path)
  #Consider pixels != NaN
  geogrid ['aoi_area'] =  stat_df[stat_df.columns.to_list()[1:] ].sum(axis=1)

  geogrid ['aoi_area'] = geogrid ['aoi_area']*100*0.0001

  columnsdate = [col for col in geogrid.columns.to_list()[1:]  if str(year) in col]

  gdfproportion = geogrid.copy()
  gdfproportion[columnsdate] = gdfproportion[columnsdate].fillna(0)
#   gdfproportion[columnsdate] / gdfproportion ['aoi_area']

  for i  in columnsdate :
      gdfproportion [i] = gdfproportion [ i ] / gdfproportion ['aoi_area']

  gdfproportion.to_file( proportion_path)

  return gdfproportion
  
  
#----------------------------------------------------------------------
#  cumulative  data
# --------------------------------------------------------------------
    
def cumulative(df,year, savepath):
    '''
    '''
    columnsdate = [col for col in df.columns.to_list() if str(year) in col]
    df = df.copy()
    df = df.fillna(0)
    list_ = columnsdate 
    for i in range(len(list_)):
         if i == 0:
             df[list_[i]] = df[list_[i]]
         else:
             df[list_[i]] = df[list_[i - 1]] + df[list_[i]]
    df.to_csv(savepath)
    gdf1 = gpd.GeoDataFrame(df, geometry='geometry')
    file = savepath.split('.')[0]
    gdf1.to_file(file+'.geojson',  driver = 'GeoJSON' )
    return df
    
    
    

#================================================================================================
#                              GROUP EVERYTHING
#----------------------------------------------------------------------------------------------=--


def update_df(season, output_path, rgb_img, grid_path, area_name, operation, year, map_path):
    
    savepath = output_path + "/dataframe/final_result"
    # Create a folder for cumulative data if there is not.
    create_folder(savepath)
    df = ''
    gdfdata= gpd.read_file(grid_path)
    if season == 'crossing_years': # Rainy season in Senegal ,We assume the rainy is from August to ...

    # #   count_ref_img1 = zonal_stats(grid_path, output_path + '/rasters/' + operation+'map.tif' ,categorical=True )
        count_ref_img1 = zonal_stats(grid_path, map_path ,categorical=True )
        stat_df1 = pd.DataFrame(count_ref_img1)
        coldoy = stat_df1.columns [1:]

        coldate =  [from_doy_to_date(x, year) if x > 111 else from_doy_to_date(x + 256, year)  for x in coldoy  ]  #A year has a maximum of 366 days, so 366% 256 = 110
    
      
        stat_df1 = stat_df1.rename( columns = dict(zip(coldoy ,  coldate)) )
        coldate.sort()
        stat_df1 =  stat_df1[coldate] * 100*0.0001
        df = stat_df1 
        df[gdfdata.columns] = gdfdata
      
    else : 
        count_ref_img1 = zonal_stats(grid_path, map_path ,categorical=True )
        stat_df1 = pd.DataFrame(count_ref_img1)
        coldoy = stat_df1.columns [1:]

        coldate =  [from_doy_to_date(x, year)  for x in coldoy  ]  #A year has a maximum of 366 days, so 366% 256 = 110

      
        stat_df1 = stat_df1.rename( columns = dict(zip(coldoy ,  coldate)) )
        coldate.sort()
        stat_df1 =  stat_df1[coldate] * 100*0.0001
      
        df = stat_df1
        df[gdfdata.columns] = gdfdata
      
      


    #-------------------------------------------------------------------------------------
    # df =  stat_df1  
    # rgb_img = '/content/drive/MyDrive/earthengine/NEWTEST/rgb_2021-01-02.tif'
 
    # Path where the proportion will be saved
    proportion_path = output_path + "/dataframe/flooding_proportion.geojson"
    #-----------

    gdfprop = create_proportion ( rgb_img, grid_path, proportion_path, year)

    gdfprop [coldate] = df[coldate]
    
    for i  in coldate :
          gdfprop [i] = gdfprop [ i ] / gdfprop ['aoi_area']
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #3.---------------------- For the cumulative ------------------------------

    # savepath = output_path + "/dataframe/cumulative"
    savepath = output_path + "/dataframe/final_result"
    # Create a folder for cumulative data if there is not.
    create_folder(savepath)
    # # Cumulative of the initial flooding output


    columnsdate = [col for col in df.columns.to_list() if str(year) in col]

    df['aoi_area'] = gdfprop ['aoi_area']
    
    df ['total_'+operation+'_area'] =  df[columnsdate].sum(axis=1)
    
    
    maxValueIndex1 = df[columnsdate].idxmax(axis=1)
    df [operation+'_date'] = maxValueIndex1 
    
    gdfprop  ['total_'+operation+'_area'] =  df[columnsdate].sum(axis=1)
    
    gdfprop [operation+'_date'] = df [operation+'_date']
    
    


    col_ = [col for col in df.columns.to_list() if str(year) not in col] 
    
    
    colss = col_ + columnsdate #[1:]



    df11 = cumulative (df[colss], year,savepath+'/' + operation + '_' + area_name + str(year)+  '.csv')

    # df11 = cumulative (df[colss], year,savepath+'/' + operation + '_'  + area_name + str(year)+  '.geojson' )

    # # Cumulative of the proportion
    df22 = cumulative (gdfprop [colss], year, savepath+'/' + operation + '_proportion_' + area_name + str(year)+  '.csv')
    # df22 = cumulative (gdfprop [colss], year, savepath+'/'+ operation + '_ proportion_'  + area_name + str(year)+  '.geojson')



def group_all ( area_name, operation, year, grid_path, images_path, output_path, rgb_img, season):
    
    
  instance1 = MainS1 ( grid_path, images_path,output_path, year, operation=operation )
  dfvh = instance1.result()
  #-------------------------------------------------------------------------------
  map_path = output_path + '/rasters/' + operation+'map.tif'
  update_df(season, output_path, rgb_img, grid_path, area_name,  operation, year, map_path)